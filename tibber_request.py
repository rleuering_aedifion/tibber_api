import tibber_credentials
import requests
import pandas as pd

url = "https://api.tibber.com/v1-beta/gql"

headers = {
    "Authorization": 'Bearer ' + tibber_credentials.token,
    "Content-Type": "application/json",
}
# today
data = {"query": tibber_credentials.query_today}
response = requests.post(url, json=data, headers=headers)
prices_today = response.json()['data']['viewer']['homes'][0]['currentSubscription']['priceInfo']['today']
# tomorrow ahead price after 1pm CET Berlin
data = {"query": tibber_credentials.query_tomorrow}
response = requests.post(url, json=data, headers=headers)
prices_tomorrow = response.json()['data']['viewer']['homes'][0]['currentSubscription']['priceInfo']['tomorrow']

if len(prices_tomorrow) > 0:
    prices = prices_today + prices_tomorrow
else:
    prices = prices_today

prices_index = []
prices_value = []
# price object to price lists
for price in prices:
    prices_index.append(price['startsAt'])
    prices_value.append(price['total'])

ts_price = pd.Series(data=prices_value, index=pd.to_datetime(prices_index).tz_convert('UTC'))
ts_price_low = ts_price[ts_price < ts_price.median()]


if False:
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates
    figure_size = (29.7 / 2.54, 21 / 2.54)
    plt.close('all')
    fig = plt.figure(0, frameon=False, figsize=figure_size)
    plt.scatter(x=ts_price.index.values, y=ts_price.values, label='expensive', color='#ff0000')
    plt.scatter(x=ts_price_low.index.values, y=ts_price_low.values, label='affordable', color='#00ff00')
    plt.legend(fontsize=14)
    plt.gca().xaxis.set_major_locator(mdates.HourLocator())
    plt.setp(plt.gca().xaxis.get_majorticklabels(), 'rotation', 90)
    plt.show()