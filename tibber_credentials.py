# demo token
token = "5K4MVS-OjfWhK_4yrjOlFe1F6kJXPVf7eQYggo8ebAE"
# robin token privat
# token = "zYgCk9FJDjfT_yBT66SnkYNevdc0_qYPqoqfNLHIHjc"

query_current = """
{
viewer {
    homes {
    currentSubscription {
        priceInfo {
        current {
            total
            energy
            tax
            startsAt
        }
        }
    }
    }
}
}
"""

query_today = """
{
viewer {
    homes {
    currentSubscription {
        priceInfo {
        today {
            total
            energy
            tax
            startsAt
        }
        }
    }
    }
}
}
"""

query_tomorrow = """
{
viewer {
    homes {
    currentSubscription {
        priceInfo {
        tomorrow {
            total
            energy
            tax
            startsAt
        }
        }
    }
    }
}
}
"""